package com.example.testing;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import com.example.testing.databinding.ActivityMainBinding;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

private ActivityMainBinding binding;

    public MainActivity() throws IOException {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

     binding = ActivityMainBinding.inflate(getLayoutInflater());
     setContentView(binding.getRoot());

        // Example of a call to a native method
        TextView tv = binding.sampleText;
        tv.setText(stringFromJNI());
        // test("test");
    }

    /**
     * A native method that is implemented by the 'testing' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();
    // public native String test(String s);

    // Used to load the 'testing' library on application startup.
    static {
        System.loadLibrary("testing");
    }

    /* Runtime r = Runtime.getRuntime();
    ProcessBuilder p = new ProcessBuilder("./micropython", "-c", "'print(\'testing\')'");
    Process process = p.start();*/
}