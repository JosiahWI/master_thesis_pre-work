#include <jni.h>
//#include "interpreter.h"

#define jmain(x) Java_com_example_testing_MainActivity_##x

JNIEXPORT jstring JNICALL jmain(stringFromJNI)(
        JNIEnv* env,
        jobject this /* this */
    ) {
    char* hello = "Hello from C";

    return (*env)->NewStringUTF(env, hello);
}

// ##########################################################################

#include "py/builtin.h"
#include "py/compile.h"
#include "py/gc.h"
#include "py/mperrno.h"
#include "py/stackctrl.h"
#include "shared/runtime/gchelper.h"
#include "shared/runtime/pyexec.h"

static char heap[4096];

JNIEXPORT jstring JNICALL jmain(main)(
        JNIEnv* env,
        jobject this,
        int argc,
        char **argv
        ) {
    mp_stack_ctrl_init();
    gc_init(heap, heap + sizeof(heap));
    mp_init();

    pyexec_friendly_repl();

    gc_sweep_all();
    mp_deinit();
    return 0;
}

void nlr_jummp_fail(void *val) {
    for (;;) {}
}

void gc_collect(void) {
    gc_collect_start();
    gc_helper_collect_regs_and_stack();
    gc_collect_end();
}

mp_import_stat_t mp_import_stat(const char *path) {
    return MP_IMPORT_STAT_NO_EXIST;
}

mp_lexer_t *mp_lexer_new_from_file(const char *filename) {
    mp_raise_OSError(MP_ENOENT);
}
