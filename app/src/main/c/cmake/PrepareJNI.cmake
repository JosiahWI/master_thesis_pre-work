find_package(JNI REQUIRED)

if (NOT TARGET JNI::JNI)
  add_library(JNI::JNI INTERFACE IMPORTED)

  target_include_directories(JNI::JNI
    INTERFACE
      ${JNI_INCLUDE_DIRS}
  )

  target_link_libraries(JNI::JNI
    INTERFACE
      ${JNI_LIBRARIES}
  )
endif()
