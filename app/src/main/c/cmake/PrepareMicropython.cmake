set(MICROPY_DIR "${CMAKE_CURRENT_SOURCE_DIR}/micropython")
set(MICROPY_PORT_DIR "${CMAKE_CURRENT_SOURCE_DIR}")

include("${MICROPY_DIR}/py/py.cmake")
include("${MICROPY_DIR}/extmod/extmod.cmake")

if (NOT TARGET micropython::micropython)
  add_library(micropython SHARED)
  add_library(micropython::micropython ALIAS micropython)
  target_sources(micropython
    PRIVATE
      ${MICROPY_SOURCES}
      "${MICROPY_DIR}/shared/readline/readline.c"
      "${MICROPY_DIR}/shared/runtime/gchelper_generic.c"
      "${MICROPY_DIR}/shared/runtime/stdout_helpers.c"
      "${MICROPY_DIR}/shared/runtime/pyexec.c"
  )

  target_include_directories(micropython
    PUBLIC
      "${CMAKE_CURRENT_BINARY_DIR}"
      "${CMAKE_CURRENT_SOURCE_DIR}"
      "${CMAKE_CURRENT_SOURCE_DIR}/micropython"
  )

  set(MICROPY_TARGET micropython)
  set(MICROPY_SOURCE_QSTR
    ${MICROPY_SOURCE_PY}
    "${MICROPY_DIR}/shared/readline/readline.c"
    "${MICROPY_DIR}/shared/runtime/gchelper_generic.c"
    "${MICROPY_DIR}/shared/runtime/pyexec.c"
    "${MICROPY_DIR}/shared/runtime/stdout_helpers.c"
  )
  include("${MICROPY_DIR}/py/mkrules.cmake")

endif()
